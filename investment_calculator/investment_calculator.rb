class InvestmentCalculator
  attr_accessor :investment_chunk, :earning_rate_per_chunk, :target_cash

  def initialize(c, f, x)
    self.investment_chunk       = c
    self.earning_rate_per_chunk = f
    self.target_cash            = x
    @initial_earning_rate       = 2.0
  end

  def time
    @time ||= time_to_earn
  end

  # invalidate the time if a dependent variable changes
  def investment_chunk=(c)
    validate_numericality(c, 'investment_chunk')
    @time = nil
    @investment_chunk = c
  end

  def earning_rate_per_chunk=(f)
    validate_numericality(f, "earning_rate_per_chunk")
    @time = nil
    @earning_rate_per_chunk = f
  end

  def target_cash=(x)
    validate_numericality(x, "target_cash")
    @time = nil
    @target_cash = x
  end

  private

  def validate_numericality(arg, label)
    unless arg.is_a? Numeric
      raise ArgumentError, "tried to set '#{arg}' for #{label}, but it's not a number."
    end
  end
  
  def time_to_earn
    error_suffix = "needed to calculate time"
    raise "investment_chunk #{error_suffix}" unless investment_chunk
    raise "earning_rate_per_chunk #{error_suffix}" unless earning_rate_per_chunk
    raise "target_cash #{error_suffix}" unless target_cash

    time         = 0
    earning_rate = @initial_earning_rate

    while true
      # how long would it take to just wait
      # at the current earning_rate?
      t_wait = target_cash / earning_rate

      # how long would it take if we made enough
      # to reinvest, then invested, then waited?

      # time to invest another chunk
      t_chunk = investment_chunk / earning_rate

      # time to finish after we invested a chunk
      t_invest = t_chunk + target_cash / (earning_rate + earning_rate_per_chunk)

      # if the time to just wait is less than
      # the time to invest then wait, we're done
      return time + t_wait if t_wait < t_invest

      # otherwise, increment everything and let's try again!
      earning_rate += earning_rate_per_chunk
      time         += t_chunk
    end

    time
  end
end
