require 'spec_helper'

describe InvestmentCalculator do
  describe "initializer" do
    before :each do
      @calculator = InvestmentCalculator.new(1, 2, 3)
    end

    it "sets the investmnt_chunk on init" do
      expect(@calculator.investment_chunk).to equal 1
    end

    it "sets the earning_rate_per_chunk on init" do
      expect(@calculator.earning_rate_per_chunk).to equal 2
    end

    it "sets the target_cash on init" do
      expect(@calculator.target_cash).to equal 3
    end
  end

  describe "getters and setters" do
    before :each do
      @calculator = InvestmentCalculator.new(0, 0, 0)
    end

    it "sets investment_chunk" do
      @calculator.investment_chunk = 9
      expect(@calculator.investment_chunk).to equal 9
    end

    it "sets earning_rate_per_chunk" do
      @calculator.earning_rate_per_chunk = 8
      expect(@calculator.earning_rate_per_chunk).to equal 8
    end

    it "sets target_cash" do
      @calculator.target_cash = 7
      expect(@calculator.target_cash).to equal 7
    end

    it "raises an error if investment_chunk is not a number" do
      expect{@calculator.investment_chunk = "potato"}.to raise_error ArgumentError
    end

    it "raises an error if earning_rate_per_chunk is not a number" do
      expect{@calculator.earning_rate_per_chunk = "rofl"}.to raise_error ArgumentError
    end

    it "raises an error if investment_chunk is not a number" do
      expect{@calculator.investment_chunk = "lol"}.to raise_error ArgumentError
    end
  end

  describe "calculator" do
    before :each do
      @calculator = InvestmentCalculator.new(30, 1, 2)
    end

    it "raises an exception if investment_chunk is missing" do
      @calculator.investment_chunk = nil

      expect{@calculator.time}.to raise_error "investment_chunk needed to calculate time"
    end

    it "raises an exception if earning_rate_per_chunk is missing" do
      @calculator.earning_rate_per_chunk = nil

      expect{@calculator.time}.to raise_error "earning_rate_per_chunk needed to calculate time"
    end

    it "raises an exception if target_cash is missing" do
      @calculator.target_cash = nil

      expect{@calculator.time}.to raise_error "target_cash needed to calculate time"
    end
  end

  describe "test data" do
    before :all do
      @data = YAML.load_file(File.join(File.dirname(__FILE__), './test_cases.yml'))
    end

    it "creates objects from loaded data" do
      @data.each do |trial|
        expect(InvestmentCalculator.new(*trial[:input])).to be_kind_of(InvestmentCalculator)
      end
    end

    it "calculates the expected time" do
      @data.each do |trial|
        expect(InvestmentCalculator.new(*trial[:input]).time).to be_within(0.001).of(trial[:output])
      end
    end
  end
end
