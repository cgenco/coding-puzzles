# invoke with:
# ruby 1_words_test.rb dictionary.txt

sequences = {}
ARGF.each_line{|word|
  word.chomp!
  letters = word.gsub(/\W/, '')

  next unless letters.length >= 4

  # get each sequence of 4 characters
  letters.split(//).each_with_index{|c, i|
    # if we can't get four characters out of the
    # end of this word, we're done
    break if letters.length - i < 4

    seq = letters[i...i+4]

    # keep track of the words that contain this sequence
    (sequences[seq] ||= []) << word
  }
}

sequences_file = File.open('sequences', 'w')
words_file     = File.open('words', 'w')

sequences.each{|sequence, words|
  # if this sequence is unique in our dictionary,
  # write it to the files
  if words.size == 1
    sequences_file.puts sequence
    words_file.puts words.first
  end
}

sequences_file.close
words_file.close

# Sources used:
# http://stackoverflow.com/questions/1628673/ruby-regex-for-a-split-every-four-characters-not-working
# http://ruby-doc.org/core-2.0/String.html#method-i-scan
# http://stackoverflow.com/questions/4795447/rubys-file-open-and-the-need-for-f-close
